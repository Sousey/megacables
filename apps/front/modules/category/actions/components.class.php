<?php
/**
 * Categoria components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 * 
 * 
 */
class categoryComponents extends myFrontModuleComponents
{

  public function executeList(dmWebRequest $request)
  {
    $this->categoryPager = $this->getPageRecord()->getNode()->getChildren();
    $this->current = $this->getPageRecord();
    /*$query = Doctrine::getTable('Category')
          ->createQuery('c')
          ->where('c.level = ?', 0);
    
    $this->categoryPager = $this->getPager($query);*/
  }

  public function executeShow(dmWebRequest $request)
  {
    $query = $this->getShowQuery();
    
    $this->category = $this->getRecord($query);
  }

  public function executeListRoot(dmWebRequest $request)
  {
    $query = Doctrine::getTable('Category')
          ->createQuery('c')
          ->where('c.level = ?', 0)
          ->orderBy('c.position asc');
    
    $this->categoryPager = $this->getPager($query);   
    $this->categoryPager = $this->getPager($query, $request->getParameter($this->getParamName()));
    $this->categoryPager->setOption('url_params', $request->getGetParameters());
  }

  public function executeListMenu(dmWebRequest $request)
  {
    $query = Doctrine::getTable('Category')
          ->createQuery('c')
          ->where('c.level = ?', 0)
          ->orderBy('c.position asc');
    
    $this->categoryPager = $this->getPager($query);
  }


}
