<?php
/**
 * Proyecto components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 * 
 * 
 */
class newItemComponents extends myFrontModuleComponents
{

  public function executeShow(dmWebRequest $request)
  {
    $query = $this->getShowQuery();
    
    $this->newItem = $this->getRecord($query);
  }

  public function executeList(dmWebRequest $request)
  {
    $query = $this->getListQuery();
    
    $this->newItemPager = $this->getPager($query);
  }

  public function executeListHome(dmWebRequest $request)
  {
    $query = $this->getListQuery();
    
    $this->newItemPager = $this->getPager($query);
  }

  public function executeListMenu(dmWebRequest $request)
  {
    $query = $this->getListQuery();
    
    $this->newItemPager = $this->getPager($query);
  }


}
