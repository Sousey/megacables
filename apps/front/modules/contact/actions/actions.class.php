<?php
/**
 * Contacto actions
 */
class contactActions extends myFrontModuleActions
{

  public function executeFormWidget(dmWebRequest $request)
  {
    $form = new ContactForm();
        
    if ($request->hasParameter($form->getName()) && $form->bindAndValid($request))
    {
      $form->save();

      $this->getService('mail')
            ->setTemplate('contact_mail')
            ->addValues($form->getValues(), 'contact_')
            ->send();

      $this->redirectBack();
    }
    
    $this->forms['Contact'] = $form;
  }


}
