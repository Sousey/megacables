<?php
/**
 * Producto components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 * 
 * 
 * 
 * 
 */
class productComponents extends myFrontModuleComponents
{

  public function executeList(dmWebRequest $request)
  {
    $query = $this->getListQuery();
    
    $this->productPager = $this->getPager($query);
    $this->category = $this->getPageRecord();
  }

  public function executeShow(dmWebRequest $request)
  {
    $query = $this->getShowQuery();
    
    $this->product = $this->getRecord($query);
  }

  public function executeShowCvs(dmWebRequest $request)
  {
    $query = $this->getShowQuery();
    
    $csvfile = $this->getRecord($query)->getEspecificaciones()->getFullPath();
    
    $csvdata = array();
    if (($file=fopen($csvfile, 'r')) !==FALSE)
    {
      while(($data = fgetcsv($file,1000,';')) !== FALSE)
      {
        $csvdata[]=$data;
      }
    }
    $this->csvData = $csvdata;
    fclose($file);
  }

  public function executeShowButton(dmWebRequest $request)
  {
    $query = $this->getShowQuery();
    
    $this->product = $this->getRecord($query);
  }

  public function executeShowPdf(dmWebRequest $request)
  {
    $query = $this->getShowQuery();
    
    $this->product = $this->getRecord($query);
  }

  public function executeListRelated(dmWebRequest $request)
  {
    
/*
    $query = $this->getListQuery();
    $this->productPager = $this->getPager($query, $request->getParameter($this->getParamName()));
    $this->productPager->setOption('url_params', $request->getGetParameters());
*/

    $currentProduct = $this->getPageRecord();

    $query = Doctrine::getTable('Product')
    ->createQuery('p')
    ->select('p.*, RANDOM() as rand')
    ->where('p.id_category = ?', $currentProduct->id_category)
    ->orderBy('rand')
    ->andWhere('p.id != ?', $currentProduct->id);
    
    $this->productPager = $this->getPager($query, $request->getParameter($this->getParamName()));
  }


}
