<?php

require_once dirname(__FILE__).'/../lib/newItemGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/newItemGeneratorHelper.class.php';

/**
 * newItem actions.
 *
 * @package    megacables
 * @subpackage newItem
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class newItemActions extends autoNewItemActions
{
}
