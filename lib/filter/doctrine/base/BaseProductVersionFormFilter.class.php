<?php

/**
 * ProductVersion filter form base class.
 *
 * @package    megacables
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseProductVersionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {


		if($this->needsWidget('id_category')){
			$this->setWidget('id_category', new sfWidgetFormDmFilterInput());
			$this->setValidator('id_category', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('name')){
			$this->setWidget('name', new sfWidgetFormDmFilterInput());
			$this->setValidator('name', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('image')){
			$this->setWidget('image', new sfWidgetFormDmFilterInput());
			$this->setValidator('image', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('image_mini')){
			$this->setWidget('image_mini', new sfWidgetFormDmFilterInput());
			$this->setValidator('image_mini', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('descripcion')){
			$this->setWidget('descripcion', new sfWidgetFormDmFilterInput());
			$this->setValidator('descripcion', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('contruccion')){
			$this->setWidget('contruccion', new sfWidgetFormDmFilterInput());
			$this->setValidator('contruccion', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('caracteristicas')){
			$this->setWidget('caracteristicas', new sfWidgetFormDmFilterInput());
			$this->setValidator('caracteristicas', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('aplicacion')){
			$this->setWidget('aplicacion', new sfWidgetFormDmFilterInput());
			$this->setValidator('aplicacion', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('especificaciones')){
			$this->setWidget('especificaciones', new sfWidgetFormDmFilterInput());
			$this->setValidator('especificaciones', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('ficha')){
			$this->setWidget('ficha', new sfWidgetFormDmFilterInput());
			$this->setValidator('ficha', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('is_active')){
			$this->setWidget('is_active', new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))));
			$this->setValidator('is_active', new sfValidatorChoice(array('required' => false, 'choices' => array(0, 1))));
		}
		if($this->needsWidget('is_new')){
			$this->setWidget('is_new', new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))));
			$this->setValidator('is_new', new sfValidatorChoice(array('required' => false, 'choices' => array(0, 1))));
		}
		if($this->needsWidget('is_promo')){
			$this->setWidget('is_promo', new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))));
			$this->setValidator('is_promo', new sfValidatorChoice(array('required' => false, 'choices' => array(0, 1))));
		}
		if($this->needsWidget('created_by')){
			$this->setWidget('created_by', new sfWidgetFormDmFilterInput());
			$this->setValidator('created_by', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('updated_by')){
			$this->setWidget('updated_by', new sfWidgetFormDmFilterInput());
			$this->setValidator('updated_by', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('position')){
			$this->setWidget('position', new sfWidgetFormDmFilterInput());
			$this->setValidator('position', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('created_at')){
			$this->setWidget('created_at', new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))));
			$this->setValidator('created_at', new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['created_at']->getOption('choices')))));
		}
		if($this->needsWidget('updated_at')){
			$this->setWidget('updated_at', new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))));
			$this->setValidator('updated_at', new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['updated_at']->getOption('choices')))));
		}
		if($this->needsWidget('version')){
			$this->setWidget('version', new sfWidgetFormDmFilterInput());
			$this->setValidator('version', new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'ProductVersion', 'column' => 'version')));
		}



		if($this->needsWidget('product_list')){
			$this->setWidget('product_list', new sfWidgetFormDoctrineChoice(array('multiple' => false, 'model' => 'Product', 'expanded' => false)));
			$this->setValidator('product_list', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'Product', 'required' => true)));
		}

    

    $this->widgetSchema->setNameFormat('product_version_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ProductVersion';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'id_category'      => 'Number',
      'name'             => 'Text',
      'image'            => 'Number',
      'image_mini'       => 'Number',
      'descripcion'      => 'Text',
      'contruccion'      => 'Text',
      'caracteristicas'  => 'Text',
      'aplicacion'       => 'Text',
      'especificaciones' => 'Number',
      'ficha'            => 'Number',
      'is_active'        => 'Boolean',
      'is_new'           => 'Boolean',
      'is_promo'         => 'Boolean',
      'created_by'       => 'Number',
      'updated_by'       => 'Number',
      'position'         => 'Number',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
      'version'          => 'Number',
    );
  }
}
