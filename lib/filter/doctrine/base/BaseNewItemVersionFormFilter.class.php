<?php

/**
 * NewItemVersion filter form base class.
 *
 * @package    megacables
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseNewItemVersionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {


		if($this->needsWidget('title')){
			$this->setWidget('title', new sfWidgetFormDmFilterInput());
			$this->setValidator('title', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('short_text')){
			$this->setWidget('short_text', new sfWidgetFormDmFilterInput());
			$this->setValidator('short_text', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('long_text')){
			$this->setWidget('long_text', new sfWidgetFormDmFilterInput());
			$this->setValidator('long_text', new sfValidatorSchemaFilter('text', new sfValidatorString(array('required' => false))));
		}
		if($this->needsWidget('image')){
			$this->setWidget('image', new sfWidgetFormDmFilterInput());
			$this->setValidator('image', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('created_at')){
			$this->setWidget('created_at', new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))));
			$this->setValidator('created_at', new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['created_at']->getOption('choices')))));
		}
		if($this->needsWidget('updated_at')){
			$this->setWidget('updated_at', new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))));
			$this->setValidator('updated_at', new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['updated_at']->getOption('choices')))));
		}
		if($this->needsWidget('created_by')){
			$this->setWidget('created_by', new sfWidgetFormDmFilterInput());
			$this->setValidator('created_by', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('updated_by')){
			$this->setWidget('updated_by', new sfWidgetFormDmFilterInput());
			$this->setValidator('updated_by', new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))));
		}
		if($this->needsWidget('version')){
			$this->setWidget('version', new sfWidgetFormDmFilterInput());
			$this->setValidator('version', new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'NewItemVersion', 'column' => 'version')));
		}



		if($this->needsWidget('new_item_list')){
			$this->setWidget('new_item_list', new sfWidgetFormDoctrineChoice(array('multiple' => false, 'model' => 'NewItem', 'expanded' => false)));
			$this->setValidator('new_item_list', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'NewItem', 'required' => true)));
		}

    

    $this->widgetSchema->setNameFormat('new_item_version_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'NewItemVersion';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'title'      => 'Text',
      'short_text' => 'Text',
      'long_text'  => 'Text',
      'image'      => 'Number',
      'created_at' => 'Date',
      'updated_at' => 'Date',
      'created_by' => 'Number',
      'updated_by' => 'Number',
      'version'    => 'Number',
    );
  }
}
