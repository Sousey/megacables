<?php

/**
 * NewItemVersion form base class.
 *
 * @method NewItemVersion getObject() Returns the current form's model object
 *
 * @package    megacables
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 * @generator  Diem 1.1 RC
 * @gen-file   /home/guena/www/megacablesa/dm/dmCorePlugin/data/generator/dmDoctrineForm/default/template/sfDoctrineFormGeneratedTemplate.php */
abstract class BaseNewItemVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    parent::setup();

		//column
		if($this->needsWidget('title')){
			$this->setWidget('title', new sfWidgetFormInputText());
			$this->setValidator('title', new sfValidatorString(array('max_length' => 150)));
		}
		//column
		if($this->needsWidget('short_text')){
			$this->setWidget('short_text', new sfWidgetFormTextarea());
			$this->setValidator('short_text', new sfValidatorString(array('max_length' => 1000, 'required' => false)));
		}
		//column
		if($this->needsWidget('long_text')){
			$this->setWidget('long_text', new sfWidgetFormTextarea());
			$this->setValidator('long_text', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('image')){
			$this->setWidget('image', new sfWidgetFormInputText());
			$this->setValidator('image', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('created_at')){
			$this->setWidget('created_at', new sfWidgetFormDateTime());
			$this->setValidator('created_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('updated_at')){
			$this->setWidget('updated_at', new sfWidgetFormDateTime());
			$this->setValidator('updated_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('created_by')){
			$this->setWidget('created_by', new sfWidgetFormInputText());
			$this->setValidator('created_by', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('updated_by')){
			$this->setWidget('updated_by', new sfWidgetFormInputText());
			$this->setValidator('updated_by', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('version')){
			$this->setWidget('version', new sfWidgetFormInputHidden());
			$this->setValidator('version', new sfValidatorChoice(array('choices' => array($this->getObject()->get('version')), 'empty_value' => $this->getObject()->get('version'), 'required' => false)));
		}



		//one to one
		if($this->needsWidget('id')){
			$this->setWidget('id', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'NewItem', 'expanded' => false)));
			$this->setValidator('id', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'NewItem', 'required' => false)));
		}




    $this->widgetSchema->setNameFormat('new_item_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'NewItemVersion';
  }

}
