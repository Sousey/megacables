<?php

/**
 * ProductVersion form base class.
 *
 * @method ProductVersion getObject() Returns the current form's model object
 *
 * @package    megacables
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 * @generator  Diem 1.1 RC
 * @gen-file   /home/guena/www/megacablesa/dm/dmCorePlugin/data/generator/dmDoctrineForm/default/template/sfDoctrineFormGeneratedTemplate.php */
abstract class BaseProductVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    parent::setup();

		//column
		if($this->needsWidget('id_category')){
			$this->setWidget('id_category', new sfWidgetFormInputText());
			$this->setValidator('id_category', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('name')){
			$this->setWidget('name', new sfWidgetFormInputText());
			$this->setValidator('name', new sfValidatorString(array('max_length' => 150)));
		}
		//column
		if($this->needsWidget('image')){
			$this->setWidget('image', new sfWidgetFormInputText());
			$this->setValidator('image', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('image_mini')){
			$this->setWidget('image_mini', new sfWidgetFormInputText());
			$this->setValidator('image_mini', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('descripcion')){
			$this->setWidget('descripcion', new sfWidgetFormTextarea());
			$this->setValidator('descripcion', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('contruccion')){
			$this->setWidget('contruccion', new sfWidgetFormTextarea());
			$this->setValidator('contruccion', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('caracteristicas')){
			$this->setWidget('caracteristicas', new sfWidgetFormTextarea());
			$this->setValidator('caracteristicas', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('aplicacion')){
			$this->setWidget('aplicacion', new sfWidgetFormTextarea());
			$this->setValidator('aplicacion', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('especificaciones')){
			$this->setWidget('especificaciones', new sfWidgetFormInputText());
			$this->setValidator('especificaciones', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('ficha')){
			$this->setWidget('ficha', new sfWidgetFormInputText());
			$this->setValidator('ficha', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('is_active')){
			$this->setWidget('is_active', new sfWidgetFormInputCheckbox());
			$this->setValidator('is_active', new sfValidatorBoolean(array('required' => false)));
		}
		//column
		if($this->needsWidget('is_new')){
			$this->setWidget('is_new', new sfWidgetFormInputCheckbox());
			$this->setValidator('is_new', new sfValidatorBoolean(array('required' => false)));
		}
		//column
		if($this->needsWidget('is_promo')){
			$this->setWidget('is_promo', new sfWidgetFormInputCheckbox());
			$this->setValidator('is_promo', new sfValidatorBoolean(array('required' => false)));
		}
		//column
		if($this->needsWidget('created_by')){
			$this->setWidget('created_by', new sfWidgetFormInputText());
			$this->setValidator('created_by', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('updated_by')){
			$this->setWidget('updated_by', new sfWidgetFormInputText());
			$this->setValidator('updated_by', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('position')){
			$this->setWidget('position', new sfWidgetFormInputText());
			$this->setValidator('position', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('created_at')){
			$this->setWidget('created_at', new sfWidgetFormDateTime());
			$this->setValidator('created_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('updated_at')){
			$this->setWidget('updated_at', new sfWidgetFormDateTime());
			$this->setValidator('updated_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('version')){
			$this->setWidget('version', new sfWidgetFormInputHidden());
			$this->setValidator('version', new sfValidatorChoice(array('choices' => array($this->getObject()->get('version')), 'empty_value' => $this->getObject()->get('version'), 'required' => false)));
		}



		//one to one
		if($this->needsWidget('id')){
			$this->setWidget('id', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'Product', 'expanded' => false)));
			$this->setValidator('id', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'Product', 'required' => false)));
		}




    $this->widgetSchema->setNameFormat('product_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'ProductVersion';
  }

}
