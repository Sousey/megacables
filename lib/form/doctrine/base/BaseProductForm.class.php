<?php

/**
 * Product form base class.
 *
 * @method Product getObject() Returns the current form's model object
 *
 * @package    megacables
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 * @generator  Diem 1.1 RC
 * @gen-file   /home/guena/www/megacablesa/dm/dmCorePlugin/data/generator/dmDoctrineForm/default/template/sfDoctrineFormGeneratedTemplate.php */
abstract class BaseProductForm extends BaseFormDoctrine
{
  public function setup()
  {
    parent::setup();

		//column
		if($this->needsWidget('id')){
			$this->setWidget('id', new sfWidgetFormInputHidden());
			$this->setValidator('id', new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)));
		}
		//column
		if($this->needsWidget('name')){
			$this->setWidget('name', new sfWidgetFormInputText());
			$this->setValidator('name', new sfValidatorString(array('max_length' => 150)));
		}
		//column
		if($this->needsWidget('descripcion')){
			$this->setWidget('descripcion', new sfWidgetFormTextarea());
			$this->setValidator('descripcion', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('contruccion')){
			$this->setWidget('contruccion', new sfWidgetFormTextarea());
			$this->setValidator('contruccion', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('caracteristicas')){
			$this->setWidget('caracteristicas', new sfWidgetFormTextarea());
			$this->setValidator('caracteristicas', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('aplicacion')){
			$this->setWidget('aplicacion', new sfWidgetFormTextarea());
			$this->setValidator('aplicacion', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('is_active')){
			$this->setWidget('is_active', new sfWidgetFormInputCheckbox());
			$this->setValidator('is_active', new sfValidatorBoolean(array('required' => false)));
		}
		//column
		if($this->needsWidget('is_new')){
			$this->setWidget('is_new', new sfWidgetFormInputCheckbox());
			$this->setValidator('is_new', new sfValidatorBoolean(array('required' => false)));
		}
		//column
		if($this->needsWidget('is_promo')){
			$this->setWidget('is_promo', new sfWidgetFormInputCheckbox());
			$this->setValidator('is_promo', new sfValidatorBoolean(array('required' => false)));
		}
		//column
		if($this->needsWidget('position')){
			$this->setWidget('position', new sfWidgetFormInputText());
			$this->setValidator('position', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('created_at')){
			$this->setWidget('created_at', new sfWidgetFormDateTime());
			$this->setValidator('created_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('updated_at')){
			$this->setWidget('updated_at', new sfWidgetFormDateTime());
			$this->setValidator('updated_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('version')){
			$this->setWidget('version', new sfWidgetFormInputText());
			$this->setValidator('version', new sfValidatorInteger(array('required' => false)));
		}


		//one to many
		if($this->needsWidget('version_list')){
			$this->setWidget('version_list', new sfWidgetFormDmPaginatedDoctrineChoice(array('multiple' => true, 'model' => 'ProductVersion', 'expanded' => true)));
			$this->setValidator('version_list', new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'ProductVersion', 'required' => false)));
		}

		//one to one
		if($this->needsWidget('image')){
			$this->setWidget('image', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'expanded' => false)));
			$this->setValidator('image', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('image_mini')){
			$this->setWidget('image_mini', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'expanded' => false)));
			$this->setValidator('image_mini', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('especificaciones')){
			$this->setWidget('especificaciones', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'expanded' => false)));
			$this->setValidator('especificaciones', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('ficha')){
			$this->setWidget('ficha', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'expanded' => false)));
			$this->setValidator('ficha', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('id_category')){
			$this->setWidget('id_category', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'Category', 'expanded' => false)));
			$this->setValidator('id_category', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'Category', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('created_by')){
			$this->setWidget('created_by', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'expanded' => false)));
			$this->setValidator('created_by', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('updated_by')){
			$this->setWidget('updated_by', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'expanded' => false)));
			$this->setValidator('updated_by', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'required' => false)));
		}



    /*
     * Embed Media form for image
     */
    if($this->needsWidget('image')){
      $this->embedForm('image_form', $this->createMediaFormForImage());
      unset($this['image']);
    }
    /*
     * Embed Media form for image_mini
     */
    if($this->needsWidget('image_mini')){
      $this->embedForm('image_mini_form', $this->createMediaFormForImageMini());
      unset($this['image_mini']);
    }
    /*
     * Embed Media form for especificaciones
     */
    if($this->needsWidget('especificaciones')){
      $this->embedForm('especificaciones_form', $this->createMediaFormForEspecificaciones());
      unset($this['especificaciones']);
    }
    /*
     * Embed Media form for ficha
     */
    if($this->needsWidget('ficha')){
      $this->embedForm('ficha_form', $this->createMediaFormForFicha());
      unset($this['ficha']);
    }

    $this->widgetSchema->setNameFormat('product[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'), $this);
  }
  /**
   * Creates a DmMediaForm instance for image_mini
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImageMini()
  {
    return DmMediaForRecordForm::factory($this->object, 'image_mini', 'ImageMini', $this->validatorSchema['image_mini']->getOption('required'), $this);
  }
  /**
   * Creates a DmMediaForm instance for especificaciones
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForEspecificaciones()
  {
    return DmMediaForRecordForm::factory($this->object, 'especificaciones', 'Especificaciones', $this->validatorSchema['especificaciones']->getOption('required'), $this);
  }
  /**
   * Creates a DmMediaForm instance for ficha
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForFicha()
  {
    return DmMediaForRecordForm::factory($this->object, 'ficha', 'Ficha', $this->validatorSchema['ficha']->getOption('required'), $this);
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image_mini');
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'especificaciones');
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'ficha');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image_mini');
    $values = $this->processValuesForEmbeddedMediaForm($values, 'especificaciones');
    $values = $this->processValuesForEmbeddedMediaForm($values, 'ficha');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image_mini', 'ImageMini');
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'especificaciones', 'Especificaciones');
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'ficha', 'Ficha');
  }

  public function getModelName()
  {
    return 'Product';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['version_list']))
    {
        $this->setDefault('version_list', array_merge((array)$this->getDefault('version_list'),$this->object->Version->getPrimaryKeys()));
    }

  }

  protected function doSave($con = null)
  {
    $this->saveVersionList($con);

    parent::doSave($con);
  }

  public function saveVersionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['version_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Version->getPrimaryKeys();
    $values = $this->getValue('version_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Version', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Version', array_values($link));
    }
  }

}
