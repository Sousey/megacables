<?php

/**
 * NewItem form base class.
 *
 * @method NewItem getObject() Returns the current form's model object
 *
 * @package    megacables
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 * @generator  Diem 1.1 RC
 * @gen-file   /home/guena/www/megacablesa/dm/dmCorePlugin/data/generator/dmDoctrineForm/default/template/sfDoctrineFormGeneratedTemplate.php */
abstract class BaseNewItemForm extends BaseFormDoctrine
{
  public function setup()
  {
    parent::setup();

		//column
		if($this->needsWidget('id')){
			$this->setWidget('id', new sfWidgetFormInputHidden());
			$this->setValidator('id', new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)));
		}
		//column
		if($this->needsWidget('title')){
			$this->setWidget('title', new sfWidgetFormInputText());
			$this->setValidator('title', new sfValidatorString(array('max_length' => 150)));
		}
		//column
		if($this->needsWidget('short_text')){
			$this->setWidget('short_text', new sfWidgetFormTextarea());
			$this->setValidator('short_text', new sfValidatorString(array('max_length' => 1000, 'required' => false)));
		}
		//column
		if($this->needsWidget('long_text')){
			$this->setWidget('long_text', new sfWidgetFormTextarea());
			$this->setValidator('long_text', new sfValidatorString(array('required' => false)));
		}
		//column
		if($this->needsWidget('created_at')){
			$this->setWidget('created_at', new sfWidgetFormDateTime());
			$this->setValidator('created_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('updated_at')){
			$this->setWidget('updated_at', new sfWidgetFormDateTime());
			$this->setValidator('updated_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('version')){
			$this->setWidget('version', new sfWidgetFormInputText());
			$this->setValidator('version', new sfValidatorInteger(array('required' => false)));
		}


		//one to many
		if($this->needsWidget('version_list')){
			$this->setWidget('version_list', new sfWidgetFormDmPaginatedDoctrineChoice(array('multiple' => true, 'model' => 'NewItemVersion', 'expanded' => true)));
			$this->setValidator('version_list', new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'NewItemVersion', 'required' => false)));
		}
		//one to many
		if($this->needsWidget('new_item_dm_media_list')){
			$this->setWidget('new_item_dm_media_list', new sfWidgetFormDmPaginatedDoctrineChoice(array('multiple' => true, 'model' => 'NewItemDmMedia', 'expanded' => true)));
			$this->setValidator('new_item_dm_media_list', new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'NewItemDmMedia', 'required' => false)));
		}

		//one to one
		if($this->needsWidget('image')){
			$this->setWidget('image', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'expanded' => false)));
			$this->setValidator('image', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmMedia', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('created_by')){
			$this->setWidget('created_by', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'expanded' => false)));
			$this->setValidator('created_by', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'required' => false)));
		}
		//one to one
		if($this->needsWidget('updated_by')){
			$this->setWidget('updated_by', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'expanded' => false)));
			$this->setValidator('updated_by', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'DmUser', 'required' => false)));
		}



    /*
     * Embed Media form for image
     */
    if($this->needsWidget('image')){
      $this->embedForm('image_form', $this->createMediaFormForImage());
      unset($this['image']);
    }

    $this->widgetSchema->setNameFormat('new_item[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'), $this);
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
  }

  public function getModelName()
  {
    return 'NewItem';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['medias_list']))
    {
        $this->setDefault('medias_list', array_merge((array)$this->getDefault('medias_list'),$this->object->Medias->getPrimaryKeys()));
    }

    if (isset($this->widgetSchema['version_list']))
    {
        $this->setDefault('version_list', array_merge((array)$this->getDefault('version_list'),$this->object->Version->getPrimaryKeys()));
    }

    if (isset($this->widgetSchema['new_item_dm_media_list']))
    {
        $this->setDefault('new_item_dm_media_list', array_merge((array)$this->getDefault('new_item_dm_media_list'),$this->object->NewItemDmMedia->getPrimaryKeys()));
    }

  }

  protected function doSave($con = null)
  {
    $this->saveMediasList($con);
    $this->saveVersionList($con);
    $this->saveNewItemDmMediaList($con);

    parent::doSave($con);
  }

  public function saveMediasList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['medias_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Medias->getPrimaryKeys();
    $values = $this->getValue('medias_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Medias', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Medias', array_values($link));
    }
  }

  public function saveVersionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['version_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Version->getPrimaryKeys();
    $values = $this->getValue('version_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Version', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Version', array_values($link));
    }
  }

  public function saveNewItemDmMediaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['new_item_dm_media_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->NewItemDmMedia->getPrimaryKeys();
    $values = $this->getValue('new_item_dm_media_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('NewItemDmMedia', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('NewItemDmMedia', array_values($link));
    }
  }

}
