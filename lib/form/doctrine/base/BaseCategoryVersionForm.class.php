<?php

/**
 * CategoryVersion form base class.
 *
 * @method CategoryVersion getObject() Returns the current form's model object
 *
 * @package    megacables
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 * @generator  Diem 1.1 RC
 * @gen-file   /home/guena/www/megacablesa/dm/dmCorePlugin/data/generator/dmDoctrineForm/default/template/sfDoctrineFormGeneratedTemplate.php */
abstract class BaseCategoryVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    parent::setup();

		//column
		if($this->needsWidget('name')){
			$this->setWidget('name', new sfWidgetFormInputText());
			$this->setValidator('name', new sfValidatorString(array('max_length' => 150)));
		}
		//column
		if($this->needsWidget('image')){
			$this->setWidget('image', new sfWidgetFormInputText());
			$this->setValidator('image', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('is_active')){
			$this->setWidget('is_active', new sfWidgetFormInputCheckbox());
			$this->setValidator('is_active', new sfValidatorBoolean(array('required' => false)));
		}
		//column
		if($this->needsWidget('root_id')){
			$this->setWidget('root_id', new sfWidgetFormInputText());
			$this->setValidator('root_id', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('lft')){
			$this->setWidget('lft', new sfWidgetFormInputText());
			$this->setValidator('lft', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('rgt')){
			$this->setWidget('rgt', new sfWidgetFormInputText());
			$this->setValidator('rgt', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('level')){
			$this->setWidget('level', new sfWidgetFormInputText());
			$this->setValidator('level', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('created_by')){
			$this->setWidget('created_by', new sfWidgetFormInputText());
			$this->setValidator('created_by', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('updated_by')){
			$this->setWidget('updated_by', new sfWidgetFormInputText());
			$this->setValidator('updated_by', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('position')){
			$this->setWidget('position', new sfWidgetFormInputText());
			$this->setValidator('position', new sfValidatorInteger(array('required' => false)));
		}
		//column
		if($this->needsWidget('created_at')){
			$this->setWidget('created_at', new sfWidgetFormDateTime());
			$this->setValidator('created_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('updated_at')){
			$this->setWidget('updated_at', new sfWidgetFormDateTime());
			$this->setValidator('updated_at', new sfValidatorDateTime());
		}
		//column
		if($this->needsWidget('version')){
			$this->setWidget('version', new sfWidgetFormInputHidden());
			$this->setValidator('version', new sfValidatorChoice(array('choices' => array($this->getObject()->get('version')), 'empty_value' => $this->getObject()->get('version'), 'required' => false)));
		}



		//one to one
		if($this->needsWidget('id')){
			$this->setWidget('id', new sfWidgetFormDmDoctrineChoice(array('multiple' => false, 'model' => 'Category', 'expanded' => false)));
			$this->setValidator('id', new sfValidatorDoctrineChoice(array('multiple' => false, 'model' => 'Category', 'required' => false)));
		}




    $this->widgetSchema->setNameFormat('category_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'CategoryVersion';
  }

}
