<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version6 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('product', 'product_image_mini_dm_media_id', array(
             'name' => 'product_image_mini_dm_media_id',
             'local' => 'image_mini',
             'foreign' => 'id',
             'foreignTable' => 'dm_media',
             ));
        $this->addIndex('product', 'product_image_mini', array(
             'fields' => 
             array(
              0 => 'image_mini',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('product', 'product_image_mini_dm_media_id');
        $this->removeIndex('product', 'product_image_mini', array(
             'fields' => 
             array(
              0 => 'image_mini',
             ),
             ));
    }
}