<?php

//require_once '/home/megacablesacom/dm/dmCorePlugin/lib/core/dm.php';
require_once '/home/guena/www/megacablesa/dm/dmCorePlugin/lib/core/dm.php';
//require_once '/vagrant/www/megacablesa/dm/dmCorePlugin/lib/core/dm.php';
dm::start();

class ProjectConfiguration extends dmProjectConfiguration
{

  public function setup()
  {
    parent::setup();
    
    $this->enablePlugins(array(
      // add plugins you want to enable here
    ));

    $this->setWebDir(sfConfig::get('sf_root_dir').'/public_html');
  }
  
}
