<?php
/**
 * @author TheCelavi
 */
class dmVegasView extends dmBehaviorBaseView {
    
    public function configure() {
		parent::configure();
		$this->addRequiredVar(array('medias', 'use_overlay', 'overlay'));
       
    }

    protected function filterBehaviorVars(array $vars = array()) {
        $vars = parent::filterBehaviorVars($vars);
        $backgrounds=array();
        $mediaIds = array();
		foreach($vars['medias'] as $index => $mediaConfig)
		{
			$mediaIds[] = $mediaConfig['id'];
		}
        $mediaRecords = empty($mediaIds) ? array() : $this->getMediaQuery($mediaIds)->fetchRecords()->getData();
        
        $this->mediaPositions = array_flip($mediaIds);
		usort($mediaRecords, array($this, 'sortRecordsCallback'));
		
		foreach($mediaRecords as $index => $mediaRecord)
		{
			$mediaTag = $this->getHelper()->media($mediaRecord);
  
			$backgrounds[] = array(
				'src'   => $mediaTag->getSrc(),
				'fade'  => $vars['medias'][$index]['transition']
				);
		}
  
		// replace media configuration by media tags
		$vars['backgrounds'] = $backgrounds;
        return $vars;
		}
    
    protected function sortRecordsCallback(DmMedia $a, DmMedia $b)
	{
		return $this->mediaPositions[$a->get('id')] > $this->mediaPositions[$b->get('id')];
	}
    
	protected function getMediaQuery($mediaIds)
	{
		return dmDb::query('DmMedia m')
			->leftJoin('m.Folder f')
			->whereIn('m.id', $mediaIds);
	}
  
    public function getJavascripts() {
        return array_merge(
            parent::getJavascripts(),            
            array(
                'dmVegasBehaviorPlugin.vegas',
                'dmVegasBehaviorPlugin.launch',
                )
        );
    } 
    
    public function getStylesheets() {
        return array_merge(
            parent::getStylesheets(),
            array(
                'dmVegasBehaviorPlugin.vegas'
            )
        );
    }
    
}

