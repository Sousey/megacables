<?php
/**
 * @author TheCelavi
 */
class dmVegasForm extends dmBehaviorBaseForm {
    protected $overlay = array(
        '/dmVegasBehaviorPlugin/img/overlays/01.png' => '01',
        '/dmVegasBehaviorPlugin/img/overlays/02.png' => '02',
        '/dmVegasBehaviorPlugin/img/overlays/03.png' => '03',
        '/dmVegasBehaviorPlugin/img/overlays/04.png' => '04',
        '/dmVegasBehaviorPlugin/img/overlays/05.png' => '05',
        '/dmVegasBehaviorPlugin/img/overlays/06.png' => '06',
        '/dmVegasBehaviorPlugin/img/overlays/07.png' => '07',
        '/dmVegasBehaviorPlugin/img/overlays/08.png' => '08',
        '/dmVegasBehaviorPlugin/img/overlays/09.png' => '09',
        '/dmVegasBehaviorPlugin/img/overlays/10.png' => '10',
        '/dmVegasBehaviorPlugin/img/overlays/11.png' => '11',
        '/dmVegasBehaviorPlugin/img/overlays/12.png' => '12',
        '/dmVegasBehaviorPlugin/img/overlays/13.png' => '13',
        '/dmVegasBehaviorPlugin/img/overlays/14.png' => '14',
        '/dmVegasBehaviorPlugin/img/overlays/15.png' => '15',
    ); 
   
    public function configure() {
		
		

       $this->widgetSchema['media_id'] = new sfWidgetFormDoctrineChoice(array(
      'model'    => 'DmMedia',
      'multiple' => true
    ));
    
    $this->validatorSchema['media_id'] = new sfValidatorDoctrineChoice(array(
      'model'    => 'DmMedia',
      'multiple' => true
    ));
     if (!$this->getDefault('medias'))
    {
      $this->setDefault('medias', array());
    }
    
     $this->validatorSchema['widget_width'] = new sfValidatorPass();
     $this->validatorSchema['media_transition'] = new sfValidatorPass();
     
    $this->validatorSchema['media_position'] = new sfValidatorPass();
        
        $this->widgetSchema['use_overlay'] = new sfWidgetFormInputCheckbox();
        $this->validatorSchema['use_overlay'] = new sfValidatorBoolean();
        
        $this->widgetSchema['preload'] = new sfWidgetFormInputCheckbox();
        $this->validatorSchema['preload'] = new sfValidatorBoolean();
        
        $this->widgetSchema['delay'] = new sfWidgetFormInputText();
        $this->validatorSchema['delay'] = new sfValidatorPass();
        
        
        
        $this->widgetSchema['overlay'] = new sfWidgetFormChoice(array(
            'choices'=>$this->getI18n()->translateArray($this->overlay)));
        $this->validatorSchema['overlay'] = new sfValidatorChoice(array(
            'choices'=> array_keys($this->overlay)));
     if (!$this->getDefault('delay'))
    {
      $this->setDefault('delay', 5000);
    }
        parent::configure();
    }
    public function getStylesheets()
  {
    return array(
      'lib.ui-tabs',
      'dmVegasBehaviorPlugin.form'
    );
  }

  public function getJavascripts()
  {
    return array(
      'lib.ui-tabs',
      'core.tabForm',
      'dmVegasBehaviorPlugin.form'
    );
  }
  
    protected function renderContent($attributes)
    {
		return $this->getHelper()->renderPartial('dmVegasBehavior', 'form', array(
		'form' => $this,
		'medias' => $this->getMedias(),
		'baseTabId' => 'dm_vegas_behavior_'.$this->dmBehavior->get('id')
    ));
   }
   protected function getMedias()
  {
    // extract media ids
    $mediaConfigs = $this->getValueOrDefault('medias');
    $mediaIds = array();
    foreach($mediaConfigs as $index => $mediaConfig)
    {
      $mediaIds[] = $mediaConfig['id'];
    }
    
    // fetch media records
    $mediaRecords = empty($mediaIds) ? array() : $this->getMediaQuery($mediaIds)->fetchRecords()->getData();
    
    // sort records
    $this->mediaPositions = array_flip($mediaIds);
    usort($mediaRecords, array($this, 'sortRecordsCallback'));
    
    // build media tags
    $medias = array();
    foreach($mediaRecords as $index => $mediaRecord)
    {
      $medias[] = array(
        'id'     => $mediaRecord->id,
        'transition'    => $mediaConfigs[$index]['transition']
        );
    }
    
    return $medias;
  }

  protected function sortRecordsCallback(DmMedia $a, DmMedia $b)
  {
    return $this->mediaPositions[$a->get('id')] > $this->mediaPositions[$b->get('id')];
  }
  
  protected function getMediaQuery($mediaIds)
  {
    return dmDb::query('DmMedia m')
    ->leftJoin('m.Folder f')
    ->whereIn('m.id', $mediaIds);
  }
   public function getBehaviorValues()
  {
    $values = parent::getBehaviorValues();
    
    $values['medias'] = array();
    
    foreach($values['media_id'] as $index => $mediaId)
    {
      $values['medias'][] = array(
        'id'   => $mediaId,
        'transition'    => $values['media_transition'][$index]
      );
    }
    
    if (empty($values['width']))
    {
      if ($values['widget_width'])
      {
        $values['width'] = $values['widget_width'];
      }
      else
      {
        $values['width'] = 300;
      }
      
      $values['height'] = dmArray::get($values, 'height', (int) ($values['width'] * 2/3));
    }
    elseif (empty($values['height']))
    {
      $values['height'] = (int) ($values['width'] * 2/3);
    }
    
    unset($values['widget_width'], $values['media_position'], $values['media_id']);
    
    return $values;
  }

}

