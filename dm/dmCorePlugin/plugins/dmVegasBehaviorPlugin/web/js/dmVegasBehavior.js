;(function($) {  


    $.extend($.dm.behaviors, {        
        dmVegasBehavior: {
            init: function(behavior) {
            },
            start: function(behavior) {
				$.vegas('slideshow',behavior);
				if (behavior.use_overlay)
				{
					$.vegas('overlay',{'src': behavior.overlay});
				}
            },
            stop: function(behavior) {
				$.vegas('stop');
            },
            destroy: function(behavior) {
				$.vegas('destroy','overlay');
            }
        }
    });
    
})(jQuery);
