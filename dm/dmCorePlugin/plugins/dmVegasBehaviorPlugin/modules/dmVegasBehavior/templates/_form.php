<?php
echo

$form->renderGlobalErrors(),

_open('div.dm_tabbed_form'),

_tag('ul.tabs',
  _tag('li', _link('#'.$baseTabId.'_medias')->text(__('Medias'))).
  _tag('li', _link('#'.$baseTabId.'_options')->text(__('Options')))
),

_tag('div#'.$baseTabId.'_medias.drop_zone',
  _tag('ol.medias_list', array('json' => array(
    'medias' => $medias,
    'delete_message' => __('Remove this media')
  )), '').
  _tag('div.dm_help.no_margin', __('Drag & drop images here from the right MEDIA panel')).
 _tag('ul',
    
    _tag('li.dm_form_element.fx.clearfix',$form['dm_behavior_enabled']->label()->field()->error())
    
    )),
_tag('div#'.$baseTabId.'_options',
	_tag('li.dm_form_element.fx.clearfix',$form['use_overlay']->label()->field()->error()).
    _tag('li.dm_form_element.fx.clearfix',$form['overlay']->label()->field()->error()).
    _tag('li.dm_form_element.fx.clearfix',$form['preload']->label()->field()->error()).
    _tag('li.dm_form_element.fx.clearfix',$form['delay']->label()->field()->error())
),
    


_close('div'); //div.dm_tabbed_form
